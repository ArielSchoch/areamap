using System;
using System.Collections.Generic;
using UnityEngine;

namespace AreaMap
{
    public class AreaMapGenerator : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        public Material generationMaterial;
        public Material displayMaterial;
        public Vector2 textureDimensions;
        public float radiusScale = 1f;
        public bool updateTextureOnStart = true;

        public static AreaMapGenerator staticInstance = null;

        // This value should be the same as MAX_AREA_UNITS in AreaMapGeneration shader
        private const int MaxAreaMapUnits = 32;

        private Material displayMaterialInstance;
        private RenderTexture renderTexture;
        private bool updateRequired;

        private readonly List<AreaMapUnit> units = new List<AreaMapUnit>();
        private readonly Vector4[] positions = new Vector4[MaxAreaMapUnits];
        private readonly float[] radii = new float[MaxAreaMapUnits];

        private static readonly int areaTexPropertyId = Shader.PropertyToID("_AreaTex");
        private static readonly int positionsPropertyId = Shader.PropertyToID("_Positions");
        private static readonly int radiiPropertyId = Shader.PropertyToID("_Radii");

        private void Awake()
        {
            staticInstance = this;
            // Avoid changes being kept after runtime
            this.displayMaterialInstance = new Material(this.displayMaterial);
            this.spriteRenderer.material = this.displayMaterialInstance;

            LogSupportedFormats();

            var textureFormat = RenderTextureFormat.Default;
            if (SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.R16))
                textureFormat = RenderTextureFormat.R16;
            else if (SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.RG16))
                textureFormat = RenderTextureFormat.RG16;
            else if (SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.RFloat))
                textureFormat = RenderTextureFormat.RFloat;

            this.renderTexture = new RenderTexture((int) this.textureDimensions.x, (int) this.textureDimensions.y, 0, textureFormat);
            this.renderTexture.filterMode = FilterMode.Trilinear;
            this.renderTexture.name = "Area Map Render Texture";
            this.renderTexture.Create();
            this.displayMaterialInstance.SetTexture(areaTexPropertyId, this.renderTexture);
        }

        private void LogSupportedFormats()
        {
            foreach (var format in (RenderTextureFormat[]) Enum.GetValues(typeof(RenderTextureFormat)))
            {
                Debug.Log($"Support for {format.ToString()}: {SystemInfo.SupportsRenderTextureFormat(format)}");
            }
        }

        private void OnDestroy()
        {
            this.renderTexture.Release();
        }

        private void Start()
        {
            if (this.updateTextureOnStart)
                UpdateTexture();
        }

        private void LateUpdate()
        {
            if (!this.updateRequired)
                return;

            for (var i = 0; i < MaxAreaMapUnits; i++)
            {
                var isDefault = i >= this.units.Count;
                this.positions[i] = isDefault ? default : this.units[i].circleArea.position;
                this.radii[i] = isDefault ? default : this.units[i].circleArea.radius * this.radiusScale;
            }

            this.generationMaterial.SetVectorArray(positionsPropertyId, this.positions);
            this.generationMaterial.SetFloatArray(radiiPropertyId, this.radii);

            // Use shader to compute area map based on unit positions and store the result in render texture
            Graphics.Blit(null, this.renderTexture, this.generationMaterial);
            this.updateRequired = false;
        }

        public void UpdateTexture()
        {
            this.updateRequired = true;
        }

        public void Register(AreaMapUnit unit)
        {
            if (this.units.Count >= MaxAreaMapUnits)
            {
                Debug.LogWarning($"Max number of area map units exceeded ({MaxAreaMapUnits}). This unit will not be rendered." +
                                 "Consider increasing MaxAreaMapUnits in AreaMapGenerator.cs");
                return;
            }

            this.units.Add(unit);
            UpdateTexture();
        }

        public void Unregister(AreaMapUnit unit)
        {
            this.units.Remove(unit);
            UpdateTexture();
        }
    }
}