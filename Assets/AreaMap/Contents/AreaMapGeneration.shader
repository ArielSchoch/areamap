Shader "AreaMap/AreaMapGeneration"
{
    Properties
    {
        _BorderSharpness ("Border Sharpness", Range(0, 20)) = 8
    }
    SubShader
    {
        Lighting Off
        Cull Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            // This value should be the same as MaxAreaMapUnits in AreaMapGenerator.cs
            static const int MAX_AREA_UNITS = 32;

            float _BorderSharpness;
            float4 _Positions[MAX_AREA_UNITS];
            float _Radii[MAX_AREA_UNITS];

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(v2f IN) : COLOR
            {
                float minDistance = 1.0;
                for (int i = 0; i < MAX_AREA_UNITS; i++)
                {
                    float4 pos = _Positions[i];
                    float radius = _Radii[i];
                    float dist = saturate((distance(pos, IN.uv.xy) - radius) * _BorderSharpness + 0.5);

                    if (dist < minDistance)
                        minDistance = dist;

                    if (minDistance <= 0.0)
                        break;
                }
                return minDistance;
            }
            ENDCG
        }
    }
}