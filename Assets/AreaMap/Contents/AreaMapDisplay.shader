﻿Shader "AreaMap/AreaMapDisplay"
{
    Properties
    {
        _AreaTex ("Area Map", 2D) = "white" {}
        _PatternTex ("Pattern", 2D) = "white" {}
        _EdgeSharpness ("Edge Sharpness", Range(0, 1000)) = 300
        _Color ("Tint", Color) = (1,1,1,1)
        _EdgeColor ("Edge color", Color) = (0,0,0,1)
        _Opacity ("Opacity", Range(0, 1)) = 1
        [MaterialToggle]
        _UseConstantEdgeSize ("Use Constant Edge Size", Int) = 0
        _ConstantEdgeScale ("Constant Edge Size Scale", Range(0, 20)) = 6.0
        [MaterialToggle]
        _RenderAreaMap ("Render Area Map", Int) = 0
    }
    SubShader
    {
        Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uvPattern : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            static const float CUTOFF = 0.5;

            sampler2D _AreaTex;
            float4 _AreaTex_ST;
            sampler2D _PatternTex;
            float4 _PatternTex_ST;

            fixed _EdgeSharpness;
            fixed4 _Color;
            fixed4 _EdgeColor;
            fixed _Opacity;
            bool _UseConstantEdgeSize;
            fixed _ConstantEdgeScale;
            bool _RenderAreaMap;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _AreaTex);
                o.uvPattern = TRANSFORM_TEX(v.uv, _PatternTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                _EdgeSharpness *= lerp(1.0, _ConstantEdgeScale / unity_OrthoParams.y, _UseConstantEdgeSize);

                fixed4 mapColor = tex2D(_AreaTex, i.uv);
                fixed4 output = tex2D(_PatternTex, i.uvPattern) * _Color;
                // Make sure only pixels inside area borders are colored
                output.a *= step(mapColor.r, CUTOFF);

                // Choose whether this pixel is part of an edge based on deviation from CUTOFF value.
                // Using the deviation as an exponent we get a bell curve which gives a smooth transition at edges (anti-aliasing).
                fixed edgeBlend = exp(-pow(_EdgeSharpness * (mapColor.r - CUTOFF), 2));
                output = lerp(output, _EdgeColor, edgeBlend);

                // If _RenderAreaMap is enabled then just draw the area map directly for debug purposes
                fixed4 final = lerp(output, mapColor, _RenderAreaMap);

                // Fade the final color
                final.a *= _Opacity;
                return final;
            }
            ENDCG
        }
    }
}
