﻿using UnityEngine;

namespace AreaMap
{
    public class CircleArea
    {
        [Range(0f, 1f)]
        public float radius;
        public Vector2 position;
    }

    public abstract class AreaMapUnit : MonoBehaviour
    {
        public CircleArea circleArea = new CircleArea();

        private void OnEnable()
        {
            AreaMapGenerator.staticInstance.Register(this);
        }

        private void OnDisable()
        {
            AreaMapGenerator.staticInstance.Unregister(this);
        }

        public void UpdatePosition(Vector2 position)
        {
            this.circleArea.position = NormalizePosition(position);
            AreaMapGenerator.staticInstance.UpdateTexture();
        }

        public void UpdateNormalizedPosition(Vector2 position)
        {
            this.circleArea.position = position;
            AreaMapGenerator.staticInstance.UpdateTexture();
        }

        public void UpdateRadius(float radius)
        {
            this.circleArea.radius = radius;
            AreaMapGenerator.staticInstance.UpdateTexture();
        }

        // Implement this method to convert from world position to UV space (0-1 range)
        public abstract Vector2 NormalizePosition(Vector2 pos);
    }
}