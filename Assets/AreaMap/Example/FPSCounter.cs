﻿using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    public Text text;

    private void Update()
    {
        Application.targetFrameRate = -1;
        this.text.text = (1f / Time.unscaledDeltaTime).ToString("00");
    }
}
