using UnityEngine;
using AreaMap;

//  Example usage of AreaUnit class
public class BuildingArea : AreaMapUnit
{
    [Range(0f, 0.2f)]
    public float radiusScale = 0.05f;

    private Vector3 startPos;
    private float speedX;
    private float speedY;

    private void Start()
    {
        UpdateRadius(this.radiusScale * this.transform.localScale.x);
        UpdatePosition(this.transform.localPosition);

        this.startPos = this.transform.localPosition;
        this.speedX = Random.Range(0.5f, 2f);
        this.speedY = Random.Range(0.5f, 2f);
    }

    private void Update()
    {
        this.transform.localPosition = this.startPos + new Vector3(Mathf.Sin(Time.time * this.speedX), Mathf.Sin(Time.time * this.speedY), 0f);

        if (this.transform.hasChanged)
        {
            UpdatePosition(this.transform.localPosition);
            this.transform.hasChanged = false;
        }
    }

    [ContextMenu("Change Radius")]
    public void ChangeRadius()
    {
        UpdateRadius(this.radiusScale * Random.Range(0.5f, 2f));
    }

    public override Vector2 NormalizePosition(Vector2 pos) => (pos / 12f) + Vector2.one * 0.5f;
}
